package jutils.jy89hui.Slick2DTools;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ComponentManager {

	private int screenWidth;
	private int screenHeight;
	private Color backgroundColor = new Color(0,0,0);
	
	private ArrayList<VisualComponent> components;
	public ComponentManager(){
		this(new ArrayList<VisualComponent>());
	}
	public ComponentManager(ArrayList<VisualComponent> components){
		this.setComponents(components);
	}
	public ArrayList<VisualComponent> getComponents() {
		return components;
	}
	public void clearCompoenents(){
		this.components=new ArrayList<VisualComponent>();
	}
	public void setComponents(ArrayList<VisualComponent> components) {
		this.components = components;
	}
	public void add(VisualComponent vc){
		this.components.add(vc);
	}
	public void render(GameContainer gc, Graphics g){
		g.setColor(backgroundColor);
		g.fillRect(0, 0, screenWidth, screenHeight);
		for(VisualComponent toTest : components){
			toTest.render(gc,g);
		}
	}
	public void update(GameContainer gc){
		for(VisualComponent toTest : components){
			toTest.update(gc);
		}
	}
	public void init(GameContainer gc ){
		this.setScreenWidth(gc.getWidth());
		this.setScreenHeight(gc.getHeight());
		for(VisualComponent toTest : components){
			toTest.setGC(gc);
			toTest.init(gc);
		}
	}
	public void keyPressed(int key, char c){
		for(VisualComponent toTest : components){
			toTest.onKeyPress(key, c);
		}
	}
	public void mouseClicked(int button){
		for(VisualComponent toTest : components){
			toTest.mouseClicked(button);
		}
	}
	public void mouseWheelMoved(int val){
		
	}
	public Color getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	public VisualComponent getComponent(int componentID){
		for(VisualComponent vc : components){
			if(vc.getID() == componentID){
				return vc;
			}
		}
		return null;
	}
	public int getScreenWidth() {
		return screenWidth;
	}
	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}
	public int getScreenHeight() {
		return screenHeight;
	}
	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
	}
}
