package jutils.jy89hui.Slick2DTools;

import jutils.jy89hui.general.logger;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Button extends VisualComponent{

	private int mouseClick = -1;
	private boolean finishAnimation = false;
	private boolean doAnimation = false;
	private boolean outline = true;
	public Button(String title, int x, int y, int width, int height){
		this.setBounds(x-(width/2), y-(height/2), width, height);
		this.title=title;
		//this.setColor(new Color(250,0,40));
	}
	public Button(String title, int x, int y, int width, int height, Color color){
		this.setBounds(x-(width/2), y-(height/2), width, height);
		this.title=title;
		this.setColor(color);
		//this.setColor(new Color(250,0,40));
	}
	private final float INVERSE_SUBTRACTION = 220f;
	private float stage = 1.0f;
	private String title ="";
	@Override
	public void render(GameContainer gc, Graphics g) {
		this.initGraphics(g);
	//	this.initColor(g);
		Color newColor = new Color(getColor().r, getColor().g,getColor().b);
		newColor.r*=(stage);
		newColor.g*=(stage);
		newColor.b*=(stage);
		g.setColor(newColor);
		g.fillRoundRect(getXLoc(), getYLoc(), getWidth(), getHeight(), 10);
		if(isOutline()){
			g.setColor(Color.black);
			g.drawRoundRect(getXLoc(), getYLoc(), getWidth(), getHeight(), 10);
		}
		Color inverse = new Color((INVERSE_SUBTRACTION-getColor().getRed()), 
				(INVERSE_SUBTRACTION-getColor().getGreen()), 
				(INVERSE_SUBTRACTION-getColor().getBlue()));
		//logger.log(""+inverse.r+" "+inverse.g+" "+inverse.b);
		g.setColor(inverse);
		float width = g.getFont().getWidth(title);
		g.drawString(title, getXLoc()+(getWidth()/2)-(width/2), getYLoc()+(getHeight()/2)-(g.getFont().getLineHeight()/2));
	}

	@Override
	public void update(GameContainer gc) {
		if(stage<1.0f){
			stage+=0.02;
		}else if(finishAnimation && mouseClick>=0){
			go(mouseClick);
			mouseClick=-1;
		}
	}
	public void setDoAnimation(boolean val){
		this.doAnimation=val;
	}
	@Override
	public void init(GameContainer gc) {
		this.setGC(gc);
		
	}

	@Override
	public void onKeyPress(int key, char c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clicked(int button) {
		if(doAnimation){
			stage = 0f;
		}
		if (finishAnimation){
			mouseClick=button;
		}else{
		  go(button);
		}
	}
	public void go(int button){
		logger.log("The button "+title+" does not have an overriden go(int) method.");
	}
	
	

	public float getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}
	public boolean isOutline() {
		return outline;
	}
	public void setOutline(boolean outline) {
		this.outline = outline;
	}
	@Override
	public void mouseWheelMoved(int val) {
		// TODO Auto-generated method stub
		
	}

}
