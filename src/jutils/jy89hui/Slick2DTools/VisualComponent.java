package jutils.jy89hui.Slick2DTools;

import java.awt.Font;

import jutils.jy89hui.general.logger;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

public abstract class VisualComponent {
	public static final TrueTypeFont DEFAULT_FONT =  new TrueTypeFont(new Font("Pt Serif", Font.PLAIN, 18),false);
	private boolean selected;
	private GameContainer gc;
	private int ID=-1;
	private int xloc,yloc,width,height;
	private Color color = Color.white;
	private TrueTypeFont font = DEFAULT_FONT;
	public void setGC(GameContainer gameContainer){
		gc = gameContainer;
	}
	public void setBounds(int x, int y, int w,int h){
		setXLoc(x);
		setYLoc(y);
		setWidth(w);
		setHeight(h);
	}
	public Color getColor(){
		return this.color;
	}
	public void setColor(Color other){
		this.color=other;
	}
	
	public void initGraphics(Graphics g){
		g.setColor(color);
		g.setFont(font);
	}
	
	public float getMouseXFloat(){
		return gc.getInput().getMouseX();
	}
	public int getMouseX(){
		return gc.getInput().getMouseX();
	}
	public float getMouseYFloat(){
		return gc.getInput().getMouseY();
	}
	public int getMouseY(){
		return gc.getInput().getMouseY();
	}
	public boolean inBounds(int x, int y){
		if (getXLoc() < x && x < getXLoc()+getWidth()){
			if (getYLoc() < y && y < getYLoc()+getHeight()){
				return true;
			}
		}
		return false;
	}
	public boolean isMouseHovering(){
		int x = getMouseX();
		int y = getMouseY();
		return inBounds(x,y);
	}
	/**
	 * Dont touch this.
	 */
	public void mouseClicked(int button){
		if (isMouseHovering()){
			if (button==0){
				this.setSelected(true);
			}
			this.clicked(button);
			return;
		}
		this.setSelected(false);
	}
	
	
	public boolean isSelected(){
		return selected;
	}
	protected void setSelected(boolean val){
		this.selected=val;
	}
	
	public abstract void render(GameContainer gc, Graphics g);
	public abstract void update(GameContainer gc);
	public abstract void init(GameContainer gc);
	public abstract void onKeyPress(int key, char c);
	public abstract void mouseWheelMoved(int val);
	public abstract void clicked(int button);
	public int getXLoc() {
		return xloc;
	}
	public void setXLoc(int xloc) {
		this.xloc = xloc;
	}
	public int getYLoc() {
		return yloc;
	}
	public void setYLoc(int yloc) {
		this.yloc = yloc;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public TrueTypeFont getFont() {
		return font;
	}
	public void setFont(TrueTypeFont font) {
		this.font = font;
	}
	public void setFont(Font font) {
		this.font = new TrueTypeFont(font,false);
	}

}
