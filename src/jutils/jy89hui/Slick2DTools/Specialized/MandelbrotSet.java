package jutils.jy89hui.Slick2DTools.Specialized;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import jutils.jy89hui.Slick2DTools.ComponentManager;
import jutils.jy89hui.Slick2DTools.VisualComponent;
import jutils.jy89hui.general.ApplicationInterface;
import jutils.jy89hui.general.logger;
import jutils.jy89hui.tests.DisplayTester;

public class MandelbrotSet extends VisualComponent{

	
	private double acorn;
	private double bcorn;
	private double size;
	private boolean finishedInfo=false;
	private int iterationMax=100;
	
	public MandelbrotSet(int x, int y, int w, int h, double acorn, double bcorn, double size){
		this.setBounds(x, y, w, h);
		this.acorn=acorn;
		this.bcorn=bcorn;
		this.size=size;
	}
	@Override
	public void render(GameContainer gc, Graphics g) {
		if(finishedInfo){
			drawSingleThread(gc, g);
			g.setColor(Color.red);
			g.drawString("RECorn:"+acorn, this.getXLoc(), this.getYLoc());
			g.drawString("IMCorn:"+bcorn, this.getXLoc(), this.getYLoc()+20);
			g.drawString("Size:"+size, this.getXLoc(), this.getYLoc()+40);
			g.drawString("Iterations:"+iterationMax, this.getXLoc(), this.getYLoc()+60);
		}else{
			g.setColor(Color.red);
			g.drawString("Welcome to the Mandelbrot Set!", (int)(this.getXLoc()+(this.getWidth()/2)-150), (int)(this.getYLoc()+(this.getHeight()*0.05)));
			g.drawString("Instructions:", (int)(this.getXLoc()+20), (int)(this.getYLoc()+(this.getHeight()*0.1)));
			g.drawString("Zoom (size): P = Zoom in, O = Zoom out", (int)(this.getXLoc()+40), (int)(this.getYLoc()+(this.getHeight()*0.15)));
			g.drawString("Movement: WASD", (int)(this.getXLoc()+40), (int)(this.getYLoc()+(this.getHeight()*0.2)));
			g.drawString("Iterations: L = More iterations, K = less iterations", (int)(this.getXLoc()+40), (int)(this.getYLoc()+(this.getHeight()*0.25)));
		}
		
	}
	public void drawMultiThread(GameContainer gc, Graphics g){
		MandelbrotSetDrawer d1 = generateDrawer(gc,0,0,getWidth(),getHeight()/4);
		MandelbrotSetDrawer d2 = generateDrawer(gc,0,getHeight()/4,getWidth(),getHeight()/2);
		MandelbrotSetDrawer d3 = generateDrawer(gc,0,getHeight()/2,getWidth(),getHeight()/2+getHeight()/4);
		MandelbrotSetDrawer d4 = generateDrawer(gc,0,getHeight()/2+getHeight()/4,getWidth(),getHeight());
		d1.run();
		d2.run();
		d3.run();
		d4.run();
		while(! (d1.finished && d2.finished && d3.finished && d4.finished)) {
			ApplicationInterface.sleep(10);
		}
		drawArray(g,d1.result,0,getWidth());
		drawArray(g,d2.result,getHeight()/4,getWidth());
		drawArray(g,d3.result,getHeight()/2,getWidth());
		drawArray(g,d4.result,getHeight()/2+getHeight()/4,getWidth());
	}
	public void drawSingleThread(GameContainer gc, Graphics g){
		for (int row = 0; row < this.getHeight(); row++) {
		    for (int col = 0; col < this.getWidth(); col++) {
		        double c_re = acorn+(col - getWidth()/2.0)*size/getWidth();
		        double c_im = bcorn+(row - getHeight()/2.0)*size/getHeight();
		        double x = 0, y = 0;
		        int iteration = 0;
		        while (x*x+y*y <= 4 && iteration < iterationMax) {
		            double x_new = x*x - y*y + c_re;
		            y = 2*x*y + c_im;
		            x = x_new;
		            iteration++;
		        }
		        g.setColor(colorFunction(iteration));
		        g.fillRect(col, row, 1, 1);
		    }
		}
	}
	public MandelbrotSetDrawer generateDrawer(GameContainer gc, int colmin, int rowmin, int colmax, int rowmax){
		MandelbrotSetDrawer drawer = new MandelbrotSetDrawer();
		drawer.acorn=acorn-(rowmin/getHeight())*2.0;
		drawer.bcorn=bcorn;
		drawer.colMAX=colmax;
		drawer.colMIN=colmin;
		drawer.rowMAX=rowmax;
		drawer.rowMIN=rowmin;
		drawer.gc = gc;
		drawer.iterationMax=iterationMax;
		drawer.height=getHeight();
		drawer.width=getWidth();
		drawer.size=size;
		return drawer;
	}
	private void drawArray(Graphics g, int[] array, int starty, int width){
		int x=0;
		int y=starty;
		int point=0;
		while((point+1)<array.length){
			g.setColor(colorFunction(array[point]));
			g.fillRect(x, y, 1, 1);
			x++;
			if(x==width){
				x=0;
				y++;
			}
			point++;
		}
	}

	private Color colorFunction(int count){
		int r = (int)(((double)count/iterationMax)*50)+40;
		int g = (int)(((double)count/iterationMax)*100)+20;
		int b = (int)(((double)count/iterationMax)*250)+60;
		return new Color(r,g,b);
	}
	@Override
	public void update(GameContainer gc) {
		
	}

	@Override
	public void init(GameContainer gc) {
		
		Thread t = new Thread(){
			@Override
			public void run(){
				ApplicationInterface.sleep(7*1000);
				finishedInfo=true;
			}
		};
		t.start();
	}

	@Override
	public void onKeyPress(int key, char c) {
		double moveAmount = size/10;
		if (key == Input.KEY_P){
			size/=1.5;
		}else if(key==Input.KEY_O){
			size*=1.5;
		}
		else if ( key == Input.KEY_W){
			bcorn-=moveAmount;
		}else if ( key == Input.KEY_A){
			acorn-=moveAmount;
		}
		else if ( key == Input.KEY_S){
			bcorn+=moveAmount;
		}else if ( key == Input.KEY_D){
			acorn+=moveAmount;
		}
		else if ( key == Input.KEY_L){
			iterationMax*=1.5;
		}else if ( key == Input.KEY_K){
			iterationMax/=1.5;
		}
	}

	@Override
	public void clicked(int button) {
		this.finishedInfo=true;
	}
	@Override
	public void mouseWheelMoved(int val) {
		
	}	
}
class MandelbrotSetDrawer {
	public int rowMIN=-1;
	public int rowMAX=-1;
	public int colMIN=-1;
	public int colMAX=-1;
	public double acorn=-1;
	public double bcorn = -1;
	public double size= -1;
	public int width=-1;
	public int height = -1;
	public int iterationMax=-1;
	public boolean finished = true;
	public int[] result = null;
	GameContainer gc = null;
	public void run() {	
		int width = colMAX-colMIN;
		int height = rowMAX-rowMIN;
		result = new int[width*height];
		finished = false;
		Thread t = new Thread(){
			@Override
			public void run(){
				int entry = 0;
				for (int row = rowMIN; row < rowMAX; row++) {
				    for (int col = colMIN; col < colMAX; col++) {
				        double c_re = acorn+(col - width/2.0)*size/width;
				        double c_im = bcorn+(row - height/2.0)*size/height;
				        double x = 0, y = 0;
				        int iteration = 0;
				        while (x*x+y*y <= 4 && iteration < iterationMax) {
				            double x_new = x*x - y*y + c_re;
				            y = 2*x*y + c_im;
				            x = x_new;
				            iteration++;
				        }
				        result[entry++]=iteration;
				    }
				}
				finished = true;
			}
		};
		t.start();
		
	}

	
	
}
/*
 * 			*/
