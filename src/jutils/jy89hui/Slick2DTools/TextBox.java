package jutils.jy89hui.Slick2DTools;


import java.util.ArrayList;

import jutils.jy89hui.general.logger;
import jutils.jy89hui.objectTools.StringTools;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class TextBox extends VisualComponent{

	private String text=" ";
	private int cursorpos=0; 
	
	public TextBox( int x, int y, int width, int height){
		
		this.setBounds(x, y, width, height);
	}
	public TextBox( int x, int y, int width, int height, int ID){
		
		this.setBounds(x, y, width, height);
		this.setID(ID);
	}
	@Override
	public void render(GameContainer gc, Graphics g) {
		this.initGraphics(g);
		g.drawRect(getXLoc(), getYLoc(), getWidth(), getHeight());
		Font font = g.getFont();
		
		ArrayList<String> seperated = this.seperateStrings(text, font, getWidth()-20);
		int y=getYLoc();
			for(String s : seperated){
				g.drawString(s, getXLoc(), y);
				y+=font.getLineHeight();
			}
		int pos =0; 
		int counter  = cursorpos;
		String before ="";
		while(true){
			int length = seperated.get(pos).length();
			if(counter-length>0){
			counter -= length;
			}else{
				String s = seperated.get(pos);
				int v =0;
				while(counter>0){
					before+=s.charAt(v);
					counter--;
				}
				break;
			}
			pos++;
		}
		y = getYLoc()+font.getLineHeight()*pos;
		float textwidth = (float) font.getWidth(before);
		if(isSelected()){
			g.drawLine(getXLoc()+textwidth, y, getXLoc()+textwidth, y+font.getLineHeight());
		}
		
	}

	@Override
	public void update(GameContainer gc) {
	}

	@Override
	public void init(GameContainer gc) {
		this.setGC(gc);
		
	}

	@Override
	public void onKeyPress(int key, char c) {
		if (this.isSelected()){
			if(key == Input.KEY_ENTER){
				// yeah really have no idea what should happen here. 
			}else if (key==42 || key==54){
				// This is shift No. I dont want to add the character [Shift] as it creates problems with some fonts.
				
			}else if (key==14){
				this.delCharacter(this.cursorpos-1);
			}else if (key==Input.KEY_LEFT){
				if(cursorpos>0){
					cursorpos--;
				}
			}else if(key == Input.KEY_RIGHT){
				if(cursorpos<text.length()-1){
					cursorpos++;
				}
			}else{
				this.addCharacter(c);
			}
		}
	}
	private void addCharacter(char c){
		text+=" ";
		String end = "";
		for(int x=0; x<text.length(); x++){
			if (x==cursorpos){
				end+=c;
			}else if (x<cursorpos){
				end+=text.charAt(x);
			}else if (x>cursorpos){
				end+=text.charAt(x-1);
			}
		}
		text=end;
		cursorpos++;
	}
	private void delCharacter(int cursorpos){
		// Never delete that last character. Otherwise we have problems adding more as cursorpos and length get fucked up
		if(text.length()<=1){
			return;
		}
		String end = "";
		for(int x=0; x<text.length(); x++){
			if (x==cursorpos){
				//end+=c;
			}else if (x<cursorpos){
				end+=text.charAt(x);
			}else if (x>cursorpos){
				end+=text.charAt(x);
			}
		}
		text=end;
		this.cursorpos--;
	}

	@Override
	public void clicked(int button) {
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
	private static ArrayList<String> seperateStrings(String text, Font font, int maxwidth){
		ArrayList<String> strings = new ArrayList<String>();
		int length = text.length();
		if(font.getWidth(text)<maxwidth){
			strings.add(text);
			return strings;
		}
		String data = "";
		for (int pos=0; pos<length;pos++){
			if(font.getWidth(data) >= maxwidth){
				strings.add(data);
				data = "";
			}
			data+=text.charAt(pos);
			
		}
		strings.add(data);
		return strings;
	}
	@Override
	public void mouseWheelMoved(int val) {
		// TODO Auto-generated method stub
		
	}
}
