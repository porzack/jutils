package jutils.jy89hui.Slick2DTools;

import jutils.jy89hui.Slick2DTools.Specialized.MandelbrotSet;
import jutils.jy89hui.general.logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;



public abstract class Display extends BasicGame{

	
	public abstract void initialize(GameContainer gc);
	public abstract void postComponentDraw(GameContainer gc, Graphics g);
	public void preComponentDraw(GameContainer gc, Graphics g){}
	public abstract void postComponentUpdate(GameContainer gc);
	public void preComponentUpdate(GameContainer gc){}
	public abstract void eventKeyPressed(int key, char c);
	public abstract void eventMouseClicked(int button, int locx, int locy, int clickcount);
	public abstract void eventMouseWheelMoved(int amount);

	private int width,height;
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public Display(String title) {
		super(title);
	}
	private ComponentManager manager = new ComponentManager();
	private boolean newScreen=false;
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		preComponentDraw(gc, g);
		manager.render(gc, g);
		postComponentDraw(gc, g);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		initialize(gc);
		manager.init(gc);
	}

	@Override
	public void update(GameContainer gc, int arg1) throws SlickException {
		if(newScreen){
			manager.init(gc);
			newScreen=false;
		}
		preComponentUpdate(gc);
		manager.update(gc);
		postComponentUpdate(gc);
	}
	@Override
	public void keyPressed(int key, char c){
		this.eventKeyPressed(key, c);
		manager.keyPressed(key, c);
	}
	@Override
	public void mouseClicked(int button, int locx, int locy, int clickCount){
		this.eventMouseClicked(button, locx, locy, clickCount);
		manager.mouseClicked(button);
	}
	@Override
	public void mouseWheelMoved(int amount){
		this.eventMouseWheelMoved(amount);
		manager.mouseWheelMoved(amount);
	}
	public void setComponentManager(ComponentManager newmanager){
		manager=newmanager;
		this.newScreen=true;
	}
	public ComponentManager getComponentManager(){
		return manager;
	}
	
	
	public static AppGameContainer inialize(Display display, int width, int height){
		try{
			display.height=height;
			display.width=width;
			AppGameContainer app = new AppGameContainer(display);
			app.setDisplayMode(width, height, false);
			app.setShowFPS(true);
			new Thread(){
				public void run(){
					try {
						app.start();
					} catch (SlickException e) {
						e.printStackTrace();
					}
				}
			}.start();
			return app;
			} catch(SlickException e){
				logger.log("How the hell did this even happen?? Run(), Console.");
			}
		return null;
	}
}