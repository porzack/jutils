package jutils.jy89hui.Slick2DTools;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CheckBox extends VisualComponent{

	private final Color DEFAULT_COLOR = new Color(200,50,50);
	private boolean isChecked=false;
	public CheckBox(GameContainer gc, int x, int y, int width, int height){
		this.setGC(gc);
		this.setBounds(x, y, width, height);
		this.setColor(this.DEFAULT_COLOR);
	}
	@Override
	public void render(GameContainer gc, Graphics g) {
		this.initGraphics(g);
		g.drawRoundRect(getXLoc(), getYLoc(), getWidth(), getHeight(), 2);
		if(isChecked()){
			g.drawLine(getXLoc(), getYLoc(), getXLoc()+getWidth(), getYLoc()+getHeight());
			g.drawLine(getXLoc(), getYLoc()+getHeight(), getXLoc()+getWidth(), getYLoc());
			//g.drawLine(getXLoc(), getYLoc()+(getHeight()/2), getXLoc()+(getWidth()/2), getYLoc()+getHeight());
			//g.drawLine(getXLoc()+(getWidth()/2), getYLoc()+getHeight(), getXLoc()+getWidth(), getYLoc());
		}
		
	}

	@Override
	public void update(GameContainer gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(GameContainer gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onKeyPress(int key, char c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clicked(int button) {
	this.setChecked(!this.isChecked());
		
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	@Override
	public void mouseWheelMoved(int val) {
		// TODO Auto-generated method stub
		
	}

}
