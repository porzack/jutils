package jutils.jy89hui.Slick2DTools;

import jutils.jy89hui.general.ApplicationInterface;
import jutils.jy89hui.general.logger;



//import org.newdawn.slick.Font;
import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

public class Title extends VisualComponent{

	
	
	/*
	 * 
	 * TODO FIX ALL THIS SHIT TO ACTUALLY MAKE IT WORK
	 * SLICK FONTS ARE SHIT.
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	private boolean highlighted = false;
	private String text = "";
	public Title(String title, int x, int y, int pointsize){
		this.setBounds(x, y, -1, -1);
		this.text=title;
	}
	public Title(String title, int x, int y, int pointsize, Font font){
		this.setBounds(x, y, -1, -1);
		this.text=title;
		this.setFont(font);
	}
	
	
	@Override
	public void render(GameContainer gc, Graphics g) {
		this.initGraphics(g);
		if(highlighted){
			g.setColor(getColor());
			g.fillRoundRect(getXLoc(),getYLoc(),getWidth(),getHeight(),5);
			g.setColor(Color.black);
		}
		g.drawString(text, getXLoc(), getYLoc());
	}

	@Override
	public void update(GameContainer gc) {
	}

	@Override
	public void init(GameContainer gc) {
		this.setGC(gc);
		Graphics g = gc.getGraphics();
		this.initGraphics(g);
		this.setWidth(g.getFont().getWidth(text));
		this.setHeight(g.getFont().getLineHeight());
		int x = getXLoc()-(getWidth()/2);
		int y = getYLoc()-(getHeight());
		this.setXLoc(x);
		this.setYLoc(y);
	}

	@Override
	public void onKeyPress(int key, char c) {
		
	}

	@Override
	public void clicked(int button) {
		highlighted=true;
		Thread t = new Thread(){
			@Override
			public void run(){
				ApplicationInterface.sleep(350);
				highlighted=false;
			}
		};
		t.start();
		
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}
	@Override
	public void mouseWheelMoved(int val) {
		// TODO Auto-generated method stub
		
	}

}
