package jutils.jy89hui.Slick2DTools;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Slider extends VisualComponent{
	private float minVal;
	private float maxVal;
	private float current;
	private boolean showMinMax;
	
	public Slider(float minval, float maxval, float current, GameContainer gc, int x, int y, int width, int height){
		this.setGC(gc);
		this.setBounds(x, y, width, height);
		this.minVal=minval;
		this.maxVal=maxval;
		this.current=current;
	}

	@Override
	public void render(GameContainer gc, Graphics g) {
		this.initGraphics(g);
		g.drawLine(getXLoc(), getYLoc()+(getHeight()/2), getXLoc()+getWidth(), getYLoc()+(getHeight()/2));
		g.drawLine(getXLoc(), getYLoc(), getXLoc(), getYLoc()+getHeight());
		g.drawLine(getXLoc()+getWidth(), getYLoc(), getXLoc()+getWidth(), getYLoc()+getHeight());
		float percent = (maxVal-minVal)/current;
		float xpos = percent*getWidth();
		float thingwidth = 0.10f;
		float thingheight = 0.90f;
		g.fillRect(getXLoc()+xpos-(thingwidth*getWidth()), getYLoc(), (thingwidth*getWidth()), (thingheight*getHeight()));
		g.drawString("val: "+current, getXLoc(), getYLoc()-20);
		g.drawRect(getXLoc(), getYLoc(), getWidth(), getHeight());
	}

	@Override
	public void update(GameContainer gc) {
		//current += 0.001;
		if(this.isMouseHovering()){
			if (gc.getInput().isMouseButtonDown(0)){
				int relX = getMouseX()-getXLoc();
				//int relY = getMouseY()-getYLoc();
				float diff = maxVal-minVal;
				float percent  = getWidth()/relX;
				float absval = percent*diff;
				float val = minVal+absval;
				current=val;
				
				
				
			}
		}
		
	}

	@Override
	public void init(GameContainer gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onKeyPress(int key, char c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clicked(int button) {
		
	}

	public float getMinVal() {
		return minVal;
	}

	public void setMinVal(float minVal) {
		this.minVal = minVal;
	}

	public float getMaxVal() {
		return maxVal;
	}

	public void setMaxVal(float maxVal) {
		this.maxVal = maxVal;
	}

	public float getCurrent() {
		return current;
	}

	public void setCurrent(float current) {
		this.current = current;
	}

	public boolean isShowMinMax() {
		return showMinMax;
	}
	public void setShowMinMax(boolean showMinMax) {
		this.showMinMax = showMinMax;
	}

	@Override
	public void mouseWheelMoved(int val) {
		// TODO Auto-generated method stub
		
	}

}
