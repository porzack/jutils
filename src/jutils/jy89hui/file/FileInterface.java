package jutils.jy89hui.file;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import jutils.jy89hui.general.logger;

public class FileInterface {

	public static void APPEND(String file, String line) throws Exception {
		APPEND(new File(file), line);
	}

	public static void APPEND(File file, String line) throws Exception {
		/*
		 * UNFINISHED
		 */
		throw new Exception("Unfinished");
	}

	public static void INSERT(String file, String line, long lineNumber) throws Exception {
		INSERT(new File(file), line, lineNumber);
	}

	public static void INSERT(File file, String line, long lineNumber) throws Exception {
		/*
		 * UNFINISHED
		 */
		throw new Exception("Unfinished");
	}

	public static void WRITE(String file, ArrayList<String> lines) {
		WRITE(new File(file), toArray(lines));
	}

	public static void WRITE(String file, String[] lines) {
		WRITE(new File(file), lines);
	}

	public static void WRITE(File file, ArrayList<String> lines) {
		WRITE(file, toArray(lines));
	}

	public static void WRITE(File file, String[] lines) {
		try {
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			for (String s : lines) {
				bufferedWriter.write(s);
				bufferedWriter.newLine();
			}
			bufferedWriter.close();
		} catch (IOException ex) {
			System.out.println("Error writing to file '" + file.getName() + "'");
		}
	}

	private static String[] toArray(ArrayList<String> arry) {
		String[] sam = new String[arry.size()];
		for (int x = 0; x < arry.size(); x++) {
			sam[x] = arry.get(x);
		}
		return sam;
	}

	public static String READ_SINGLE(String file, long lineNumber) {
		return READ_SINGLE(new File(file), lineNumber);
	}

	public static String READ_SINGLE(File file, long lineNumber) {
		String line = "NULL";
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int x = 0;
			while ((line = bufferedReader.readLine()) != null) {
				if (x == lineNumber) {
					bufferedReader.close();
					return line;
				}
				x++;
			}
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + file.getName() + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + file.getName() + "'");
		}
		return line;
	}

	public static ArrayList<String> READ_BATCH(String file) {
		return READ_BATCH(new File(file));
	}

	public static ArrayList<String> READ_BATCH(File file) {
		String line = null;
		ArrayList<String> strings = new ArrayList<String>();
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while ((line = bufferedReader.readLine()) != null) {
				strings.add(line);
			}
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + file.getName() + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + file.getName() + "'");
		}
		return strings;
	}

	public static ArrayList<String> READ_BATCH_PDF(String file) {
		return READ_BATCH_PDF(new File(file));
	}

	public static ArrayList<String> READ_BATCH_PDF(File file) {
		ArrayList<String> strings = new ArrayList<String>();
		try {
			PDDocument document = null;
			document = PDDocument.load(file);
			document.getClass();
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper Tstripper = new PDFTextStripper();
				String st = Tstripper.getText(document);
				String[] split = st.split("\n");
				for (String s : split) {
					strings.add(s);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strings;
	}

	public static boolean CONTAINS(String file, String data) {
		return CONTAINS(new File(file), data);
	}

	public static boolean CONTAINS(File file, String data) {
		if (GETLINE(file, data) < 0) {
			return false;
		} else {
			return true;
		}
	}

	public static long GETLINE(String file, String data) {
		return GETLINE(new File(file), data);
	}

	public static long GETLINE(File file, String data) {
		String line = "NULL";
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int x = 0;
			while ((line = bufferedReader.readLine()) != null) {
				if (line.equals(data)) {
					bufferedReader.close();
					return x;
				}
				x++;
			}
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + file.getName() + "'");
		} catch (IOException ex) {
			System.out.println("Error reading file '" + file.getName() + "'");
		}
		return -1;
	}
	public static BufferedImage LOAD_IMAGE(String location){
		return LOAD_IMAGE(new File(location));
	}
	public static BufferedImage LOAD_IMAGE(File file){
		try {
			return ImageIO.read(file);
		} catch (IOException e) {
			logger.error("error reading requested file at: "+file.getAbsolutePath());
			e.printStackTrace();
		}
		return null;
	}

}
