package jutils.jy89hui.general;

public enum LoggerMode {
	TYPE_DEBUG,
	TYPE_ERROR,
	TYPE_INFO,
	TYPE_WARNING,
}
