package jutils.jy89hui.general;


public class ApplicationInterface {
	public static void sleep(long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void bringAppToFocus()
	{
	    LanguageInterface.AppleScript("tell me to activate");
	}
	public static long getTime() {
		return System.currentTimeMillis();
	}
	public static long getDelta(long pasttime){
		long ctime = System.currentTimeMillis();
		return ctime-pasttime;
	}
}
