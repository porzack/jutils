package jutils.jy89hui.general;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class CPUTest {
	public static long l = 0;
	public static long ONE_MILLION = 1000000;
	public static long ONE_BILLION = 1000000000;
	public static long ONE_TRILLION = 1000000000000l;
	static ArrayList<tempData> tempDataSet = new ArrayList<tempData>();
	public static long stime = System.currentTimeMillis();
	public static boolean threadLocked = false;
	public static void main(String[] args){
		for (int x=0; x<Runtime.getRuntime().availableProcessors(); x++){
			new Thread(){
				public void run(){
					for(;;){
							l+=Math.cos(l)+Math.sin(l);
							if ((l==(10*ONE_BILLION))){
								l=0;
									long etime = System.currentTimeMillis();
									long diff = (etime-stime);
									getASlot(getAdvTempOfCores()).addData(diff);
									//System.out.println(""+getAdvTempOfCores()+","+diff);
									stime = System.currentTimeMillis();
									System.out.println("CurrentData: ");
									for(tempData t : tempDataSet){
										System.out.println(t.toString());
									}
						}
					}
				}
			}.start();
		}
	}
	public static int getAdvTempOfCores(){
		try {
			String data = LanguageInterface.Bash("/Applications/TemperatureMonitor.app/Contents/MacOS/tempmonitor -a -l");
			//System.out.println(data);
			String[] individuals = data.split("\n");
			//System.out.println(individuals[5]);
			String[] toAdv = {individuals[4],individuals[5]};
			for(int x=0; x<toAdv.length; x++){
				String s= toAdv[x];
				String[] maincomps = s.split(":");
				String[] dataUnparsed = maincomps[1].split(" ");
				String parsed = dataUnparsed[1];
				toAdv[x] = parsed;
			}
			int combined = 0;
			for(int x=0; x<toAdv.length; x++){
				combined+= Integer.parseInt(toAdv[x]);
			}
			combined /= toAdv.length;
			//logger.log(combined+"");
			return combined;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.log("Had to return 0. on getAdvTempOfCores. Process failed.");
		System.exit(1);
		return 0;
	}
	public static tempData getASlot(int temp){
		for(tempData t : tempDataSet){
			if(t.temp==temp){
				return t;
			}
		}
		tempData newD = new tempData(temp);
		tempDataSet.add(newD);
		return newD;
		
	}
}
class tempData{
	public long currentData;
	public int temp;
	public tempData(int temp){
		this(temp,-1);
	}
	public tempData(int temp, int originaldata){
		this.temp=temp;
		this.currentData=originaldata;
	}
	public void addData(long d){
		if(currentData==-1){
			currentData=d;
		}else{
			currentData+=d;
			currentData/=2;
		}
	}
	public String toString(){
		return temp+","+currentData;
	}
}
