package jutils.jy89hui.general;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class LanguageInterface {
	public static ArrayList<String> BashRead(String command){
		ArrayList<String> result = new ArrayList<String>();
		Runtime rt = Runtime.getRuntime();
		logger.log("Executing: "+command+" ");
		Process proc;
		try {
			proc = rt.exec(command);
			BufferedReader stdInput = new BufferedReader(new 
				     InputStreamReader(proc.getInputStream()));
			BufferedReader stdError = new BufferedReader(new 
				     InputStreamReader(proc.getErrorStream()));
			String s = null;
			while ((s = stdInput.readLine()) != null) {
			    result.add(s);
			}
			while ((s = stdError.readLine()) != null) {
			    result.add(s);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}
	public static String Bash(String cmd) {
		 Process proc = null;
		 try {
			proc = Runtime.getRuntime().exec(cmd);
		 } catch (IOException e) {
			e.printStackTrace();
		}
	        java.io.InputStream is = proc.getInputStream();
	        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	        String val = "";
	        if (s.hasNext()) {
	            val = s.next();
	        }
	        else {
	            val = "";
	        }
	        return val;
	}
	public static String AppleScript(String script)
	{
	    ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("AppleScript");

	    try
	    {
	        return (String)engine.eval(script);
	    }
	    catch (ScriptException e)
	    {
	        e.printStackTrace();
	    }
	    return null;
	}
}
