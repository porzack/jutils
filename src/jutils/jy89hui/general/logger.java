package jutils.jy89hui.general;

import java.util.ArrayList;

import jutils.jy89hui.ConsoleManager.Console;

public class logger {

	public static <T> void log(T s){
		System.out.println("[Log] "+s);
	}
	public static void log(String s, LoggerMode l){
		System.out.println("[Log] "+s);
	}
	public static <T> void logList(ArrayList<T> s){
		for(T str:s){
			log(str);
		}
	}
	public static <T> void logArray(T... s){
		log("?");
		for(T str:s){
			log("l");
			log(str);
		}
	}
	
	public static void log(int[] s){
		log("{");
		for(int str:s){
			log(str+",");
		}
		log("}");
	}
	public static void warn(String s){
		System.out.println("[WARNING] "+s);
	}
	public static void error(String s){
		System.out.println("[ERROR] "+s);
	}
	public static void fatalError(String s){
		System.out.println("[FATAL ERROR] "+s);
		System.exit(0);
	}
	
}
