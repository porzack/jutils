package jutils.jy89hui.general;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DisplayTools {
	public static void DISPLAY_IMAGE(BufferedImage image){
		DISPLAY_IMAGE(-1,-1,-1,-1,null, image);
	}
	public static void DISPLAY_IMAGE(String title,BufferedImage image){
		DISPLAY_IMAGE(-1,-1,-1,-1,title,image);
	}
	public static void DISPLAY_IMAGE(int x, int y, int wx, int wy, BufferedImage image){
		DISPLAY_IMAGE(x,y,wx,wy,null,image);
	}
	public static void DISPLAY_IMAGE(int x, int y, int wx, int wy, String title, BufferedImage image){
		x = x==-1 ? 40 : x;
		y = y==-1 ? 40 : y;
		wx = wx==-1 ? image.getWidth() : wx;
		wy = wy==-1 ? image.getHeight() : wy;
		if(image==null){
			logger.log("Cannot draw null image.");
			return;
		}
		JFrame window = new JFrame();
		window.setBounds(x, y, wx, wy);
		window.setTitle(title);
		JPanel pane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
            }
        };
        window.add(pane);
		window.setVisible(true);
		
	}
}
