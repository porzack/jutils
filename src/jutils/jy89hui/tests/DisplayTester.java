package jutils.jy89hui.tests;

import java.util.ArrayList;
import java.util.List;

import jutils.jy89hui.Slick2DTools.ComponentManager;
import jutils.jy89hui.Slick2DTools.Display;
import jutils.jy89hui.Slick2DTools.VisualComponent;
import jutils.jy89hui.Slick2DTools.Specialized.MandelbrotSet;
import jutils.jy89hui.general.logger;

import org.lwjgl.opengl.GLContext;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class DisplayTester extends Display{

	public static void main(String[] args){
		Display.inialize(new DisplayTester("Visual Component Tester"), 900, 900);
	}
	public DisplayTester(String title) {
		super(title);
	}

	@Override
	public void initialize(GameContainer gc) {
		getComponentManager().add(new MandelbrotSet(0,0,900,900,0,0,4.0));
	}

	@Override
	public void postComponentDraw(GameContainer gc, Graphics g) {
		
	}

	@Override
	public void postComponentUpdate(GameContainer gc) {
		
	}

	@Override
	public void eventKeyPressed(int key, char c) {
	}

	@Override
	public void eventMouseClicked(int button, int locx, int locy, int clickcount) {
	}

	@Override
	public void eventMouseWheelMoved(int amount) {
	}

}