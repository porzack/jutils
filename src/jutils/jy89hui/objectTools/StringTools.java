package jutils.jy89hui.objectTools;

import java.util.ArrayList;

public class StringTools {

	public static ArrayList<String> SEPERATE_STRING(String text, int maxlength) {
		ArrayList<String> strings = new ArrayList<String>();
		int length = text.length();
		if (length < maxlength) {
			strings.add(text);
			return strings;
		}
		String data = "";
		int x = 0;
		for (int pos = 0; pos < length; pos++) {
			data += text.charAt(pos);
			if (x == maxlength) {
				x = 0;
				strings.add(data);
				data = "";
			}
			x++;
		}
		strings.add(data);
		return strings;
	}

	public static String FORM_STRING(char[] arry, int start, int end) {
		String result = "";
		for (int pos = start; pos < end; pos++) {
			result += arry[pos];
		}
		return result;
	}

	public static String FORM_STRING(String[] arry, int start, int end) {
		String result = "";
		for (int pos = start; pos < end; pos++) {
			result += arry[pos];
		}
		return result;
	}

	public static String FORM_STRING(char[] arry) {
		String result = "";
		for (char c : arry) {
			result += c;
		}
		return result;
	}
}
