package jutils.jy89hui.objectTools;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;

public class ArrayTools {

	public static int[] initialize(int[] in, int length, int val){
		in = new int[length];
		in=fill(in,val);
		return in;
	}
	public static int[] fill(int[] in, int val){
		for(int x=0; x<in.length; x++){
			in[x]=val;
		}
		return in;
	}
	public static float[] fill(float[] in, float val){
		for(int x=0; x<in.length; x++){
			in[x]=val;
		}
		return in;
	}
	public static ArrayList<String> arrayToList_STRING(String[] in){
		ArrayList<String> out = new ArrayList<String>();
		for(String s:in){
			out.add(s);
		}
		return out;
	}
	public static FloatBuffer createFloatBuffer(float[] data){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	public static ByteBuffer createByteBuffer(byte[] data){
		ByteBuffer buffer = BufferUtils.createByteBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	public static IntBuffer createIntBuffer(int[] data){
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
}
