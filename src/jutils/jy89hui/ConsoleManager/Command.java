package jutils.jy89hui.ConsoleManager;

import jutils.jy89hui.general.logger;


public class Command {

	public String cmd;
	public Command(String cmd){
		this.cmd=cmd;
	}
	public void c(String command){
		String[] data = command.split(" ");
		String[] ndata = new String[data.length-1];
		for(int x=1; x<data.length; x++){
			ndata[x-1] = data[x];
		}
		try {
		called(ndata);
		} catch (NumberFormatException e){
			logger.log("You have passed invalid arguments to the "+cmd+" command!");
		} catch( Exception e){
			logger.error("There has been an error executing your code for the command: "+cmd);
			e.printStackTrace();
		}
	}
	public void called(String[] args){
		logger.log("You forgot to override the called(string args) method for the command "+cmd);
	}
}
