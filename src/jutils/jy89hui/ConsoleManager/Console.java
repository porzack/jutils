package jutils.jy89hui.ConsoleManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import jutils.jy89hui.general.logger;

public class Console extends BasicGame{
	private String cmdLine = "";
	public static Console instance;
	public static final Color INPUT_COLOR = Color.green;
	public static final Color LOG_COLOR = Color.white;
	public static final Color WARN_COLOR = Color.orange;
	public static final Color ERROR_COLOR = Color.red;
	private ArrayList<Command> commands  = new ArrayList<Command>();
	
	protected Console(String title) {
		super(title);
		
	}
	
	private ArrayList<Line> lines = new ArrayList<Line>();
	public int lineInc = 20;
	private int cursorx=0;
	private int cursory=0;

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		int inc = lineInc;
		int height = gc.getHeight();
		int width = gc.getWidth();
		int numLines = (int) Math.ceil((double)height/inc);

		int y = height; 
		int x=1;
		y-=inc;
		while(y>0 && x<lines.size()){
			y-=inc;
			Line line = lines.get(lines.size()-x);
			g.setColor(line.color);
			g.drawString(line.text, 0, y);
			x++;
		}
		g.setColor(INPUT_COLOR);
		g.drawString(cmdLine, 0, height-(inc));
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		Console.instance.lines.add(new Line("Console. ("+month+"/"+day+"/"+year+") @ "+hour+":"+minute+":"+second));
		this.registerCommand(new Command("echo"){
			@Override
			public void called(String[] args){
				String str = "";
				for (String s : args){
					str+=s;
					str+=" ";
				}
				logger.log(str);
			}
		});
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
	}
	
	public static void main(String[] args){
		Console.createInstance("Dick", 500, 600);
	}
	@Override
	public void keyPressed(int key, char c){
		if (key==Input.KEY_ENTER){
			lines.add(new Line(Color.green, cmdLine));
			this.runThrough(cmdLine);
			cmdLine="";
		}
		// 14 is delete
		else if (key==14){
			String[] d = cmdLine.split("");
			cmdLine="";
			for(int x=0; x<d.length-1; x++){
				cmdLine+=d[x];
			}
		}else{
			cmdLine+=c;
		}
	}
	public static void createInstance(String name, int width, int height) {
			try{
			Console console = new Console(name);
			Console.instance=console;
			Console.instance.lines.add(new Line("This line is invisible."));
			AppGameContainer app = new AppGameContainer(console);
			app.setDisplayMode(width, height, false);
			app.setShowFPS(false);
			app.start();
			} catch(SlickException e){
				logger.log("How the hell did this even happen?? Run(), Console.");
			}
		}
	private void runThrough(String s){
		boolean found = false;
		for(Command c : commands){
			if (s.startsWith(c.cmd)){
				c.c(s);
				found = true;
			}
		}
		if (s.equals("exit")){
			logger.log("Exiting!");
			System.exit(0);
		}
		else if (s.equals("free")){
			logger.log("Logger Freeed");
		//	logger.free();
			found=true;
		}
		else if (s.equals("capture")){
		//	logger.capture(this);
			logger.log("Logger Captured");
			found=true;
		}
		if (!found){
			logger.log("That command does not exist!");
		}
	}
	public void log(String s){
		lines.add(new Line(LOG_COLOR,s));
	}
	public void warn(String s){
		lines.add(new Line(WARN_COLOR,s));
	}
	public void error(String s){
		lines.add(new Line(ERROR_COLOR,s));
	}
	public void log(String s,Color color){
		lines.add(new Line(color,s));
	}
	public void registerCommand(Command c){
		commands.add(c);
	}
	

	
	
	

	
	public static void SEND(long IDENTIFICATION){
		
	}
	public static ArrayList<String> RECEIVE(long IDENTIFICATION){
		return null;
	}
	
	
	
	
	
	
}
class Line{
	public Color color;
	public String text;
	Line(String s){
		this(Color.white, s);
	}
	Line(Color col, String s){
		this.color=col;
		this.text=s;
	}
}

/*
 * private ArrayList<Command> commands  = new ArrayList<Command>();
 
public void run(){
Thread t = new Thread(){
	@Override
	public void run(){
		capture();
	}
};
t.start();
}
public void registerCommand(Command c){
commands.add(c);
}
private void capture(){
Scanner scanner = new Scanner(System.in);
String in = scanner.nextLine();
runThrough(in);
}
private void runThrough(String s){
boolean found = false;
for(Command c : commands){
	if (s.startsWith(c.cmd)){
		c.c(s);
		found = true;
	}
}
if (s.equals("exit")){
	logger.log("Quitting!");
	System.exit(0);
}
if (!found){
	logger.log("That command does not exist!");
}
capture();
}

*/